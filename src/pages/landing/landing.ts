import { Component } from '@angular/core';
import { PhotoTakerPage } from '../photo-taker/photo-taker';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {
  userData: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    var self = this;
    console.log('ionViewDidLoad LandingPage');
    this.userData = this.navParams.get('userData');
    //alert("alert 2" + this.userData.first_name);
    
  }

  photoTakers(){
    //var user = this.navParams.get('userData');
    this.navCtrl.push(PhotoTakerPage, {userData: this.userData});
  }

  onload(){
  	
  }

}
