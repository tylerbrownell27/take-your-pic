import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LandingPage } from '../landing/landing';
import firebase from 'firebase';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  userData: any;
  constructor(private facebook: Facebook, public navCtrl: NavController,  private navParams: NavParams ) {

  }
// login(): Promise<any> {
//   return this.facebook.login(['email'])
//     .then( response => {
//       const facebookCredential = firebase.auth.FacebookAuthProvider
//         .credential(response.authResponse.accessToken);

//       firebase.auth().signInWithCredential(facebookCredential)
//         .then( success => { 
//           console.log("Firebase success: " + JSON.stringify(success)); 
//         });

//     }).catch((error) => { console.log(error) });
// }
  login() {
  	// let provider = new firebase.auth.FacebookAuthProvider();
  	// firebase.auth().signInWithRedirect(provider).then(()=>{
  	// 	firebase.auth().getRedirectResult().then((result)=>{
  	// 		alert(JSON.stringify(result));
  	// 	}).catch(function(error){
  	// 		alert(JSON.stringify(error));
  	// 	});
  	// });
  	 var userData = {};
  	
    this.facebook.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
      this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
        userData = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']}
      	//alert("alert 1" + userData);
      	this.navCtrl.push(LandingPage, {userData: userData});
      });
    });
    //alert("done logging in")
    
  }
}
