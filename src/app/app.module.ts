import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LandingPage } from '../pages/landing/landing';
import { PhotoTakerPage } from '../pages/photo-taker/photo-taker';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Facebook } from '@ionic-native/facebook';



import firebase  from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyCnbd7RAHZBKTSAX6a-CIg9vrf-dxAWU1E",
    authDomain: "phone-b50e6.firebaseapp.com",
    databaseURL: "https://phone-b50e6.firebaseio.com",
    projectId: "phone-b50e6",
    storageBucket: "phone-b50e6.appspot.com",
    messagingSenderId: "841520702739"
  });

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LandingPage,
    PhotoTakerPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LandingPage,
    PhotoTakerPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Facebook,
  ]
})
export class AppModule {}
